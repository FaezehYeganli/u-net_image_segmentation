from __future__ import division, print_function
#matplotlib inline
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
plt.rcParams['image.cmap'] = 'gist_earth'
np.random.seed(98765)

from tf_unet import image_gen
from tf_unet import unet
from tf_unet import util